package com.moataz.exchange.presentation.state

import com.moataz.exchange.presentation.model.CurrencyConversion

sealed class ExchangeRateDataState {
    object Initial : ExchangeRateDataState()
    object Loading : ExchangeRateDataState()
    class Success(val rates: List<CurrencyConversion>) : ExchangeRateDataState()
    class Error(val error: Throwable) : ExchangeRateDataState()
}
