package com.moataz.exchange.presentation.state

import com.moataz.exchange.presentation.model.HistoricalExchangeRateData

sealed class HistoricalExchangeRateDataState {
    object Initial : HistoricalExchangeRateDataState()
    object Loading : HistoricalExchangeRateDataState()
    class Success(val data: List<HistoricalExchangeRateData>) : HistoricalExchangeRateDataState()
    class Error(val error: Throwable) : HistoricalExchangeRateDataState()
}
