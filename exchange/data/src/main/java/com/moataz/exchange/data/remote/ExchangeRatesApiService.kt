package com.moataz.exchange.data.remote

import com.moataz.exchange.data.remote.model.ExchangeRateRemoteDTO
import retrofit2.http.GET
import retrofit2.http.Path

interface ExchangeRatesApiService {
    @GET("latest")
    suspend fun getLatestExchangeRates(): ExchangeRateRemoteDTO

    @GET("{date}")
    suspend fun getHistoricalExchangeRates(@Path("date") date: String): ExchangeRateRemoteDTO
}
