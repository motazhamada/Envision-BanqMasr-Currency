package com.moataz.exchange.data.di

import com.moataz.exchange.core.IoDispatcher
import com.moataz.exchange.core.ServiceFactory
import com.moataz.exchange.data.ExchangeRateRepository
import com.moataz.exchange.data.ExchangeRateRepositoryImpl
import com.moataz.exchange.data.remote.ExchangeRatesApiService
import com.moataz.exchange.data.remote.ExchangeRatesRemoteDataSource
import com.moataz.exchange.data.remote.ExchangeRatesRemoteDataSourceImpl
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent
import dagger.hilt.android.scopes.ViewModelScoped
import kotlinx.coroutines.CoroutineDispatcher

@Module
@InstallIn(ViewModelComponent::class)
object RemoteDataDiModule {

    @Provides
    @ViewModelScoped
    fun provideRepo(
        remoteDataSource: ExchangeRatesRemoteDataSource,
        @IoDispatcher dispatcher: CoroutineDispatcher,
    ): ExchangeRateRepository {
        return ExchangeRateRepositoryImpl(
            exchangeRatesRemoteDataSource = remoteDataSource,
            dispatcher = dispatcher,
        )
    }

    @Provides
    @ViewModelScoped
    internal fun provideDataSource(
        service: ExchangeRatesApiService,
        @IoDispatcher dispatcher: CoroutineDispatcher,
    ): ExchangeRatesRemoteDataSource =
        ExchangeRatesRemoteDataSourceImpl(dispatcher = dispatcher, service = service)

    @Provides
    @ViewModelScoped
    internal fun provideDataService(serviceFactory: ServiceFactory): ExchangeRatesApiService {
        return serviceFactory.create(ExchangeRatesApiService::class.java)
    }
}
