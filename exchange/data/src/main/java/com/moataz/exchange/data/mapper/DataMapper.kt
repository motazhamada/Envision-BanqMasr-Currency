package com.moataz.exchange.data.mapper

import com.moataz.exchange.data.model.ExchangeRateDTO
import com.moataz.exchange.data.remote.model.ExchangeRateRemoteDTO

fun ExchangeRateRemoteDTO.toDomainModel() =
    ExchangeRateDTO(
        success = success,
        date = date,
        historical = historical,
        timestamp = timestamp,
        base = base,
        rates = rates,
    )
