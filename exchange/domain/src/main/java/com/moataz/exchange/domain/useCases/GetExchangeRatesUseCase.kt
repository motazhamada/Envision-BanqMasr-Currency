package com.moataz.exchange.domain.useCases

import com.moataz.exchange.data.ExchangeRateRepository
import com.moataz.exchange.data.model.ExchangeRateDTO
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.withContext
import javax.inject.Inject

interface GetExchangeRatesUseCase {
    suspend operator fun invoke(): ExchangeRateDTO?
}

internal class GetExchangeRatesUseCaseImpl @Inject constructor(
    private val exchangeRateRepository: ExchangeRateRepository,
    private val dispatcher: CoroutineDispatcher,
) : GetExchangeRatesUseCase {

    override suspend fun invoke(): ExchangeRateDTO? {
        return withContext(dispatcher) {
            runCatching {
                exchangeRateRepository.getExchangeRates()
            }.getOrNull()
        }
    }
}
