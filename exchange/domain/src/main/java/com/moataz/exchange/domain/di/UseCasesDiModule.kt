package com.moataz.exchange.domain.di

import com.moataz.exchange.core.DefaultDispatcher
import com.moataz.exchange.data.ExchangeRateRepository
import com.moataz.exchange.domain.useCases.GetExchangeRatesUseCase
import com.moataz.exchange.domain.useCases.GetExchangeRatesUseCaseImpl
import com.moataz.exchange.domain.useCases.GetHistoricalExchangeRatesUseCase
import com.moataz.exchange.domain.useCases.GetHistoricalExchangeRatesUseCaseImpl
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent
import dagger.hilt.android.scopes.ViewModelScoped
import kotlinx.coroutines.CoroutineDispatcher

@Module
@InstallIn(ViewModelComponent::class)
object UseCasesDiModule {

    @Provides
    @ViewModelScoped
    fun provideGetExchangeRatesUseCase(
        repository: ExchangeRateRepository,
        @DefaultDispatcher dispatcher: CoroutineDispatcher,
    ): GetExchangeRatesUseCase =
        GetExchangeRatesUseCaseImpl(repository, dispatcher)

    @Provides
    @ViewModelScoped
    fun provideGetHistoricalExchangeRatesUseCase(
        repository: ExchangeRateRepository,
        @DefaultDispatcher dispatcher: CoroutineDispatcher,
    ): GetHistoricalExchangeRatesUseCase =
        GetHistoricalExchangeRatesUseCaseImpl(repository, dispatcher)
}
