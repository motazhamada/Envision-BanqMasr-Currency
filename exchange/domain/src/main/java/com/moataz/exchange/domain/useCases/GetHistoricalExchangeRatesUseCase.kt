package com.moataz.exchange.domain.useCases

import com.moataz.exchange.data.ExchangeRateRepository
import com.moataz.exchange.data.model.ExchangeRateDTO
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.withContext
import javax.inject.Inject

interface GetHistoricalExchangeRatesUseCase {
    suspend operator fun invoke(date: String): ExchangeRateDTO?
}

internal class GetHistoricalExchangeRatesUseCaseImpl @Inject constructor(
    private val exchangeRateRepository: ExchangeRateRepository,
    private val dispatcher: CoroutineDispatcher,
) : GetHistoricalExchangeRatesUseCase {

    override suspend fun invoke(date: String): ExchangeRateDTO? {
        return withContext(dispatcher) {
            runCatching {
                exchangeRateRepository.getHistoricalExchangeRates(date)
            }.getOrNull()
        }
    }
}
